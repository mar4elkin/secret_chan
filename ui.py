class Ui:
    def __init__(self, keyr):
        self.keyr = keyr

    def input_ask(self):
        print('Введите текст')
        usr_input = str(input())
        return usr_input

    def rand_key(self, keyr):
        print('Ваш ключ для сообщения ', keyr,' продолжить?\n y/n')
        usr_answer = str(input())

        if usr_answer == "y" or "Y" or "yes" or "YES" or "Да" or "да":
             return 
        else:
            return exit
