import random
import string

class Rand:
    def randomString(self,):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(8))
