from Crypto.Cipher import DES

class SecretMessage:
    def __init__(self, text, key):
        self.text = text
        self.key = key
    
    def encrypting(self, text, key):
        def pad(text):
            while len(text) % 8 != 0:
                text += b' '
            return text

        bkey = key.encode('utf-8')
        btext = text.encode('utf-8')
        des = DES.new(bkey, DES.MODE_ECB)
        padded_text = pad(btext)
        encrypted_text = des.encrypt(padded_text)
        return encrypted_text

    def decrypting(self, encrypted_text, key):
        bkey = key.encode('utf-8')
        des = DES.new(bkey, DES.MODE_ECB)
        text = des.decrypt(encrypted_text)
        text_decoded = text.decode('utf-8')
        return text_decoded
